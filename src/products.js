const products = [
    {
      product    : 'flash t-shirt',
      price      :  27.50,
      category   : 't-shirts',
      bestSeller :  false,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  true
    },
    {
      product    : 'batman t-shirt',
      price      :  22.50,
      category   : 't-shirts',
      bestSeller :  true,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  true
    },
    {
      product    : 'superman hat',
      price      :  13.90,
      category   : 'hats',
      bestSeller :  true,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  false
    }
]

export default products
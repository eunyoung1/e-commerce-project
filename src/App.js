import React from 'react';
import products from './products'
import './App.css';

function App() {

const renderAllProducts = products.map((product, i)=>{
  return <div key ={i} >
  
  {product.onSale?<p className = 'display'>on sale</p>:<p className = 'displayNot'>on sale</p>}
  <img src={product.image} alt="product.category"/>
  <h1>{product.product}</h1>
  <p>{product.price}€</p>
  
  </div>
})


// renderBestsellers
const renderBestsellers = products.map((product,i) =>{
return product.bestSeller 
     ? <div key ={i} className = 'bestSeller'>
     {product.onSale?<p className = 'display'>on sale</p>:<p className = 'displayNot'>on sale</p>}
     <img src={product.image} alt="product.category"/>
     <h1>{product.product}</h1>
     <p>{product.price}€</p>
    
     </div>
     : null

})

  return (
    <div className="App">
      <header className="App-header">
   <div className ='headerName'><h1>My eCommerce page</h1></div> 
    <div className = 'headerMenu'>
        <span>HOME</span>
        <span>SHOP</span>
        <span>BLOG</span>
        <span>CONTACT</span>
</div>
      </header>
<div className = 'menuBar'>
        <span>HOME</span>
        <span>SHOP</span>
        <span>BLOG</span>
        <span>CONTACT</span>
        <span>HOME</span>
        <span>SHOP</span>
        <span>BLOG</span>
        <span>CONTACT</span>
</div>
<div className = 'menuBar'>
        <span>HOME</span>
        <span>SHOP</span>
        <span>BLOG</span>
        <span>CONTACT</span>
        <span>HOME</span>
        <span>SHOP</span>
        <span>BLOG</span>
        <span>CONTACT</span>
</div>
    <div>
    <h2>ALL PRODUCTS</h2>
<div className = 'grid3'>{renderAllProducts}</div>
    <h2>BEST SELLER</h2>
<div className = 'grid3'>{renderBestsellers}</div>
    </div>
<footer>

</footer>
    </div>
  );
}

export default App;

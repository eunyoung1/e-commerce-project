import React from 'react'

const ProductList = () => {
  
    const products = [ 't-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes' ]
  
    const renderCategories = products.map((product, i)=>{
      return <h1 key ={i}>  {product}</h1>
    })
  
    return <div> { renderCategories } </div>
  
}

export default ProductList

